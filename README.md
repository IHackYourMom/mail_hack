```sh
docker-compose up
```

```sh
docker exec -it container_name /bin/bash
```

------

## Backend
```sh
cd type_n_help
python manage.py runserver 0.0.0.0:8000
```

## Frontend
```
cd type-n-help
yarn
yarn start
```