import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {


  constructor(){
    super()

    this.state = {
      body: '',
      tags: ''
    }

    this.handleChangeBody = this.handleChangeBody.bind(this)
    this.handleChangeTags = this.handleChangeTags.bind(this)
    this.handleReq = this.handleReq.bind(this)
  }

  handleChangeBody(event) {
   this.setState({body: event.target.value});
   console.log(event.target.value)
  }

  handleChangeTags(event) {
   this.setState({tags: event.target.value});
   console.log(event.target.value)
  }

  handleReq(){
    axios.post("http://172.20.10.3:21066/posts/new/",
    {
      "main":{
        "body": this.state.body,
        "author": 1,
        "theme": 1
      },
      "tags": this.state.tags
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">New Post</h1>
        </header>
        <div>
          <textarea
            rows="5"
            type="text"
            cols="80"
            value={this.state.body}
            onChange={this.handleChangeBody}
            required
          />
          <input
            cols="80"
            type="text"
            value={this.state.tags}
            onChange={this.handleChangeTags}
            required
          />

          <input type="submit" onClick={this.handleReq}/>

        </div>
      </div>
    );
  }
}

export default App;
