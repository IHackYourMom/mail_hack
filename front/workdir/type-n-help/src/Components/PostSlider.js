import React, { Component } from 'react'
import Slider from 'react-slick'
import Post from './Post'
import './Styles/Slider.css'
import "/workdir/type-n-help/node_modules/slick-carousel/slick/slick.css"
import "/workdir/type-n-help/node_modules/slick-carousel/slick/slick-theme.css"

class PostSlider extends Component {
  render() {

    const settings = {
      infinite: false,
      centerMode: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1
    };

    return (
      <div>
        <Slider {...settings}>
          {this.props.posts_arr.map(item => (
            <div key={item.id}>
              <Post
                text={item.body}
                tags={item.tag}
              />
            </div>
          ))}
        </Slider>
      </div>
    )
  }
}

export default PostSlider;
