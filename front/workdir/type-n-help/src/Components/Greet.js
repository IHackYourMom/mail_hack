import React, { Component } from 'react'
import './Styles/Greet.css'

//onClick={this.props.changeGreetState}

class Greet extends Component {
  render() {
    return (
      <div className="Greetings">
        <p>ОПИШИ СВОЮ ПРОБЛЕМУ ТРЕМЯ СЛОВАМИ:</p>
        <input
            type="text"
            onKeyPress={this.props.changeGreetState}
            placeholder="Начни писать..."
            value = {this.props.tags}
            onChange = {this.props.onChange}
            required
        />
      </div>
    )
  }
}

export default Greet
