import React, { Component } from 'react'
import PostSlider from './PostSlider'
import './Styles/Content.css'
/*
{this.props.isPosts ? <Post /> :
          <h1>Click 'posts'</h1>}
*/

class Content extends Component {
  render() {
    return (
      <div className="Content">
        <PostSlider
          posts_arr={this.props.posts_arr}
        />
      </div>
    )
  }
}

export default Content;
