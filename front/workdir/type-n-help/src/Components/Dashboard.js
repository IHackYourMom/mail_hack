import React, { Component } from 'react';
import './Styles/Dashboard.css'
import ReactDOM from 'react-dom'
import axios from 'axios'


class Dashboard extends Component {

  constructor() {
    super()
    this.state = {
      menu: false,
      themes: []
    }
    this.changeMenuState = this.changeMenuState.bind(this)


    axios.get("http://172.20.10.3:21066/themes/").then(response => {
      this.setState({ themes: response.data })
      console.log(response.data)
    })

  }

  changeMenuState() {
    this.setState({ menu: !this.state.menu })
  }

  componentDidMount() {
    document.addEventListener('click',
        this.handleClickOutside.bind(this), true)
  }

  componentWillUnmount() {
    document.removeEventListener('click',
        this.handleClickOutside.bind(this), true)
  }

  handleClickOutside(event) {
      const domNode = ReactDOM.findDOMNode(this);

      if ((!domNode || !domNode.contains(event.target))) {
        this.setState({
          menu: false
        });
      }
  }

  render() {
    return (
      <div>
        {this.state.menu ?
          <div>
          <div className="Dashboard">
            {this.state.themes.map(item => (
              <a>{item.name}</a>
            ))}
          </div>
          </div> :
          <div className="Button-float" onClick={this.changeMenuState}>
            <p className="Plus">☰</p>
          </div>
        }
      </div>
    )
  }
}

export default Dashboard;
