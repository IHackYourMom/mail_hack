import React, { Component } from 'react'
//import FullSizePost from './FullSizePost'
import './Styles/Post.css'

class Post extends Component {


  constructor(){
    super();
    this.state = {
      isFullsize: false
    }

    this.FullSize = this.FullSize.bind(this)

  }




  FullSize() {
    this.setState({ isFullsize: !this.state.isFullsize})
  }

  render() {
    return (
        <div className="Post">
          <p>{this.props.text}</p>
        </div>
    )
  }
}

export default Post;
