import React, { Component } from 'react';
import Dashboard from './Dashboard'
import Content from './Content'
import Greet from './Greet'
import axios from 'axios'
import './Styles/App.css'

class App extends Component {

  constructor(){
    super()
    this.state = {
      posts: false,     // Показывать посты или нет
      posts_arr: [],    // Массив постов
      greet: true,      // Показывать ли приветствие
      tags: '',         // Теги, которые ввел пользователь
    }
    this.changePostsState = this.changePostsState.bind(this)
    this.changeGreetState = this.changeGreetState.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  changePostsState(){
    this.setState({ posts: true })
  }

  onChange(e) {
    this.setState({ tags: e.target.value.toLowerCase().split(' ')})

  }

  changeGreetState(event){
    if(event.key === 'Enter') {
      this.setState({ greet: false, posts: true })

      axios.post("http://172.20.10.3:21066/posts/", {"tags": this.state.tags}).then(response => {
        this.setState({ posts_arr: response.data })
        console.log(response.data)
      })

    }
  }

  render() {
    return (
      <div>
        {this.state.greet ?
          <div className="Center-greet">
            <Greet
              changeGreetState={this.changeGreetState}
              onChange={this.onChange}
              value = {this.state.tags}
            />
          </div>
          :
          <div>
            <div className="dash">
              <Dashboard changePostsState={this.changePostsState} />
            </div>

            <div className="content">
              <Content
                isPosts={this.state.posts}
                posts_arr={this.state.posts_arr}/>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default App;
