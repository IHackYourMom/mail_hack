from rest_framework.views import APIView
from rest_framework.response import Response
from tagging.models import Tag, TaggedItem


from .models import Post, Author, Theme
from .serializers import PostSerializer, ThemeSerializer


class ThemeList(APIView):
    def get(self, request):
        themes = Theme.objects.all()
        serializer = ThemeSerializer(themes,  many=True)
        return Response(serializer.data)

class PostList(APIView):

    '''
    Структура JSON:
    {
     "tags": ["tag1", "tag2"]
    }
    '''


    def get(self, request):
        posts = Post.objects.all()
        serializer = PostSerializer(posts,  many=True)
        return Response(serializer.data)

    def post(self, request):
        print(request.data)
        #theme = request.data["theme"]
        tags = request.data["tags"]
        tags_arr = []
        #theme_data = Post.objects.filter(theme=theme)

        for i in range(len(tags)):

            if not Tag.objects.filter(name=tags[i]).exists():
                Tag(name=tags[i]).save()

            tags_arr.append(Tag.objects.get(name=tags[i]))



        objects = TaggedItem.objects.get_union_by_model(Post, tags_arr)
        serializers = PostSerializer(objects, many=True)
        return Response(serializers.data)

class PostNew(APIView):

    '''
    Структура json:
{
"main":{
"body": "some txt",
"author": 1,
"theme": 1
},
"tags": "tag1, tag2",
}
    '''

    def post(self, request):
        newPost = PostSerializer(data=request.data["main"])
        tags_arr = request.data["tags"]

        if newPost.is_valid():
            newPost.save()
            id = newPost.data["id"]

            newPostObject = Post.objects.get(pk=id)
            newPostObject.tags = tags_arr

            print(newPostObject.tags)

        else:
            print(newPost.errors)

        return Response(newPost.data)

'''
class TagList(APIView):
    def get(self, request):
        tags = Post.objects.all()
        print(Tag.objects.get_for_object(tags))
        return Response("Works")
'''

'''
Под вопросом:
Нужно ли?
class AuthorList(APIView):
    def get(self, request):
        pass

    def post(self, request):
        pass
'''
