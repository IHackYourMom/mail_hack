from django.db import models
from tagging.registry import register
from tagging.models import Tag
from datetime import datetime
from django.utils import timezone

class Author(models.Model):
    name = models.CharField('Name', max_length=50)

class Theme(models.Model):
    name = models.CharField('Name ', max_length=50)
    tag1 = models.CharField('Tag 1', max_length=50)
    tag2 = models.CharField('Tag 2', max_length=50)
    tag3 = models.CharField('Tag 3', max_length=50)

class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)
    body = models.TextField('Text')
    date = models.DateTimeField(default=timezone.now())
    rate = models.IntegerField(default=0, blank=False, null=False)
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE, null=True)

register(Post)
