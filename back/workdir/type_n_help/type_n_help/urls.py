from django.contrib import admin
from django.urls import path
from posts import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('posts/', views.PostList.as_view()),
    path('posts/new/', views.PostNew.as_view()),
    path('themes/', views.ThemeList.as_view()),
    #path('tags/', views.TagList.as_view()),
]
